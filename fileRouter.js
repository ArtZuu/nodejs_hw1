const express = require('express');
const router = express.Router();
const {createFile, getFiles, getFile, deleteFile, putFile} = require('./fileController');

router.post('/', createFile);
router.get('/', getFiles);
router.get('/:filename', getFile);
router.delete('/:filename', deleteFile);
router.put('/:filename', putFile);

module.exports = router;
