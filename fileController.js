const fs = require('fs');
const path = 'files';
const validExtension = /(\.|\/)(log|txt|json|yaml|xml|js)$/i;
const getExtension =  /\.[a-z]+$/i;
const passwords = require('./passwords.json');

function createFile(req, res) {
    try {
        if (!req.body.filename) {
            return res.status(400).send({"message": `Please specify 'filename' parameter`});
        }
        if (!req.body.content) {
            return res.status(400).send({"message": `Please specify 'content' parameter`});
        }
        if (req.body.filename && req.body.content) {
            if (req.body.password) {
                passwords[req.body.filename] = req.body.password;
                fs.writeFileSync('./passwords.json', JSON.stringify(passwords))
            }
            if(fs.existsSync(`./${path}/${req.body.filename}`)) {
                return res.status(400).send({"message": "File already exists"});
            } 
            if (req.body.filename.match(validExtension)) {
                fs.writeFileSync(`./${path}/${req.body.filename}`, req.body.content);
                return res.status(200).send({"message": "File created successfully"});
            }
            if (!req.body.filename.match(validExtension)) {
                return res.status(400).send({"message": "Invalid extension"});
            }
        }
    } catch (error) {
        return res.status(500).send({"message": "Server error"});
    }
};

function getFiles(req, res) {
    try {
        let filesArray = [];
        fs.readdirSync(`./${path}`).forEach(file => {            
            filesArray.push(file);
        });
        if (filesArray.length === 0) {
            return res.status(400).send({"message": "Client error"});
        } 
        if (filesArray.length !== 0){
            return res.status(200).send({"message": "Success", "files": filesArray});
        }
    } catch (error) {
        return res.status(500).send({"message": "Server error"});
    }
};

function getFile(req, res) {
    try {
        if(fs.existsSync(`./${path}/${req.params.filename}`)) {
            for (let key in passwords) {
                if (key === req.params.filename && !req.body.password) {
                    return res.status(400).send({"message": "Enter the password"});
                }
                if (key === req.params.filename && key.password === req.body.password) {
                    return res.status(200).send({
                        "message": "Success",
                        "filename": req.params.filename,
                        "content": fs.readFileSync(`./${path}/${req.params.filename}`, 'utf8'),
                        "extension": req.params.filename.match(getExtension)[0].slice(1),
                        "uploadedDate": fs.statSync(`./${path}/${req.params.filename}`).birthtime
                    });
                }
            }
            return res.status(200).send({
                "message": "Success",
                "filename": req.params.filename,
                "content": fs.readFileSync(`./${path}/${req.params.filename}`, 'utf8'),
                "extension": req.params.filename.match(getExtension)[0].slice(1),
                "uploadedDate": fs.statSync(`./${path}/${req.params.filename}`).birthtime
            });
        }
        if(!fs.existsSync(`./${path}/${req.params.filename}`)) {
            return res.status(400).send({"message": `No file with '${req.params.filename}' filename found`});
        }
    } catch (error) {
        return res.status(500).send({"message": "Server error"});
    }
};

function deleteFile(req, res) {
    try {
        if(fs.existsSync(`./${path}/${req.params.filename}`)) {
            let passwordsArray = fs.readFileSync('passwords.json');
            let parsedPasswords = JSON.parse(passwordsArray);
            for (let key in parsedPasswords) {
                if (key === req.params.filename) {
                    delete parsedPasswords[key];
                }
            }
            fs.writeFileSync('./passwords.json', JSON.stringify(parsedPasswords))

            fs.unlinkSync(`./${path}/${req.params.filename}`);
            return res.status(200).json({"message": "File delete successfully"});
        }
        if(!fs.existsSync(`./${path}/${req.params.filename}`)) { 
            return res.status(400).json({"message": "File do not extists"});
        }
    } catch (error) {
        return res.status(500).send({"message": "Server error"});
    }
};

function putFile(req, res) {
    try {
        if(fs.existsSync(`./${path}/${req.params.filename}`)) {
            fs.writeFileSync(`./${path}/${req.params.filename}`, req.body.content);
            return res.status(200).send({"message": `File's content have been changed to '${req.body.content}'`});
        }
        if (!fs.existsSync(`./${path}/${req.params.filename}`)) {
            return res.status(400).send({"message": "No such file in directory"});
        }
    } catch (error) {
        return res.status(500).send({"message": "Server error"});
    }
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    deleteFile,
    putFile
}
