const fs = require('fs');
const express = require("express");
const morgan = require("morgan");
const app = express();
const port = 8080;
const fileRouter = require('./fileRouter');

app.use(express.json());
app.use(morgan('combined'));

if (!fs.existsSync('./files')){
    fs.mkdirSync('./files');
}

app.use('/api/files', fileRouter);

app.listen(port);
